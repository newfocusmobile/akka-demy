package recap


object AdvancedRecap extends App {
  //  partial functions - operate on a subset of the input domain
  val partialFunction: PartialFunction[Int, Int] = {
    case 1 => 42
    case 2 => 65
    case 5 => 999
    //      if the value is anything else, function will throw exception
  }

  val pf = (x: Int) => x match {
    case 1 => 42
    case 2 => 65
    case 5 => 999
  }

  val function: Int => Int = partialFunction

  val modifiedList = List(1, 2, 3).map {
    case 1 => 42
    case _ => 0
  }

  //  lifting
  val lifted = partialFunction.lift //  turns a partial function to a total function Int => Option[Int]
  lifted(2) //  Some(65)
  //  lifted(5000) // None


  //  orElse
  val pfChain = partialFunction.orElse[Int, Int] {
    case 60 => 9000
  }

  pfChain(5) // returns 999 per the original function
  pfChain(60) //  returns 9000 per the chained partial function
  //  pfChain(457) // throws a match error

  //  type aliases
  type ReceiveFunction = PartialFunction[Any, Unit] //  the new type is an alias

  def receive: ReceiveFunction = {
    case 1 => println("Hello")
    case _ => println("I am confused")
  }

  receive(12345)

  //  implicits
  implicit val timeout = 3000

  def setTimeout(f: () => Unit)(implicit timeout: Int) = f()

  setTimeout(() => println("timeout")) // extra parameter list omitted because there is an implicit value declared before

  //  implicit conversions
  case class Person(name: String) {
    def greet = println(s"Hi, my name is $name")
  }

  implicit def fromStringToPerson(string: String): Person = Person(string)

  "Peter".greet
  fromStringToPerson("Peter").greet //same as above

  //  implicit classes
  implicit class Dog(name: String) {
    def bark = println(s"$name Woof!")
  }

  "Lassie".bark
  new Dog("Lassie").bark

  // organizing implicits
  implicit val inverseOrdering: Ordering[Int] = Ordering.fromLessThan(_ > _)
  println(List(1, 2, 3).sorted)

  //  imported scope

  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.Future

  val future = Future {
    println("hello future")
  }
}
