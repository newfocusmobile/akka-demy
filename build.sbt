name := "akka-essentials"

version := "0.1"

scalaVersion := "2.13.6"

val akkaVersion = "2.6.15"

libraryDependencies ++= Seq(
  "com.typesafe.akka"%%"akka-actor" % akkaVersion,
  "com.typesafe.akka"%%"akka-testkit" % akkaVersion,
  "org.scalatest"%%"scalatest" % "3.2.9"
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "ch.qos.logback" % "logback-classic" % "1.2.5"
)