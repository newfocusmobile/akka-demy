package actors

import akka.actor.{Actor, ActorSystem, Props}

object IntroClassic extends App {
  //  1. actor systems

  //  the actor system is a heavy weight data structure that controls a number of threads which it allocates to running actors
  //  recommended to have one actor system per application
  //  name of the actor system should only contain alphanumerics
  val actorSystem = ActorSystem("firstActorSystem")
//  println(actorSystem.name)

  //  2. creating actors

  //  actors are uniquely identified
  //  messages are passed and processed asynchronously
  //  each actor has a unique behavior
  class WordCountActor extends Actor {
    //  internal data
    var totalWords = 0

    //  behavior
    def receive: PartialFunction[Any, Unit] = {
      case message: String =>
        println(s"[word counter] I have received: $message")
        totalWords += message.split(" ").length
      case msg => println(s"[word counter] I cannot understand ${msg.toString}")
    }
  }

  //  3. Instantiate actor
  //  you can only instantiate an actor by invoking and using the actor system
  val wordCounter = actorSystem.actorOf(Props[WordCountActor], "wordCounter")
  val anotherWordCounter = actorSystem.actorOf(Props[WordCountActor], "anotherWordCounter")

  //  sending message
  wordCounter ! "I am learning Akka and its damn cool!"
  anotherWordCounter ! "A different message"


}
