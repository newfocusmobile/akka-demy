package actors

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.LoggerOps
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}

object HelloWorld {
  final case class Greet(whom: String, replyTo: ActorRef[Greeted])

  final case class Greeted(whom: String, from: ActorRef[Greet])

  def apply(): Behavior[Greet] = Behaviors.receive { (context, message) =>
    context.log.info(s"Hello ${message.whom}")
    message.replyTo ! Greeted(message.whom, context.self) //bang or tell used to send a message to another actor
    Behaviors.same //the next behavior is the same as the current one
  }
}

object HelloWorldBot {
  def apply(max: Int): Behavior[HelloWorld.Greeted] = {
    bot(0, max)
  }

  private def bot(greetingCounter: Int, max: Int): Behavior[HelloWorld.Greeted] = Behaviors.receive { (context, message) =>
    val n = greetingCounter + 1
    context.log.info(s"Greeting $n for ${message.whom}")
    if (n == max) {
      Behaviors.stopped
    } else {
      message.from ! HelloWorld.Greet(message.whom, context.self)
      bot(n, max)
    }
  }
}

object HelloWorldMain extends App {
  final case class SayHello(name: String)

  def apply(): Behavior[SayHello] = Behaviors.setup { context =>
    val greeter = context.spawn(HelloWorld(), "greeter")

    Behaviors.receiveMessage { message =>
      val replyTo = context.spawn(HelloWorldBot(max = 3), message.name)
      greeter ! HelloWorld.Greet(message.name, replyTo)
      Behaviors.same
    }
  }

  val system: ActorSystem[HelloWorldMain.SayHello] =
    ActorSystem(HelloWorldMain(), "hello")

  system ! HelloWorldMain.SayHello("World")
  system ! HelloWorldMain.SayHello("Akka")
}

