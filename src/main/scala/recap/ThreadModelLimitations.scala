package recap

object ThreadModelLimitations extends App {
  //  OOP encapsulation is only valid in a single threaded model
  //  This bank account is not thread safe
  class BankAccount(private var amount: Int) {
    override def toString: String = "" + amount

    def withdraw(money: Int): Unit = this.synchronized {
      this.amount -= money
    }

    def deposit(money: Int): Unit = this.synchronized {
      this.amount += money
    }

    def getBalance: Int = amount
  }

  val account = new BankAccount(5000)
  //  for (_ <- 1 to 1000) {
  //    new Thread(() => account.withdraw(1)).start()
  //  }
  //
  //  for (_ <- 1 to 1000) {
  //    new Thread(() => account.deposit(1)).start()
  //  }

  println(account.getBalance) //  this results in 4999 instead of 5000

  // OOP encapsulation is broken in a multithreaded env

  //  this can be solved with synchronization locks
  //  locks however create more issues - deadlocks and livelocks
  //  we therefore need a data structure that is fully encapsulated without the use of locking.

  //  delegating sth to a thread is a pain.
  //  how do you send a signal to a specific thread that is running?
  //  how do you send a runnable to a running thread?
  var task: Runnable = null
  val runningThread: Thread = new Thread(() => {
    while (true) {
      while (task == null) {
        runningThread.synchronized {
          println("[background] waiting for a task...")
          runningThread.wait()
        }
      }
      task.synchronized {
        println("[background] i have a task!")
        task.run()
        task = null
      }
    }
  })

  def delegateToBackgroundThread(r: Runnable): Unit = {
    if (task == null) task = r
    runningThread.synchronized {
      runningThread.notify()
    }
  }

  runningThread.start()
  Thread.sleep(500)
  delegateToBackgroundThread(() => println(42))
  Thread.sleep(1000)
  delegateToBackgroundThread(() => println("this should run in the background"))
}
