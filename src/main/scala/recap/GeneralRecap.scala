package recap

//extends App automatically creates a main function making this runnable
object GeneralRecap extends App {

  val aCondition: Boolean = false

  //  values cannot be reassigned
  var aVariable = 30 //inferred type integer
  aVariable += 1 //aVariable = 31

  //  expressions
  val aConditionedVal = if (aCondition) 42 else 65

  //  code block
  //  The result of a code block ist eh result of its last expression. in this example, the result is 56
  val aCodeBlock = {
    if (aCondition) 74
    56
  }

  // types
  // Unit type denotes expressions that have side effects (they do sth but don't return anything meaningful)
  val theUnit: Unit = println("Hello Scala")

  //  functions
  def aFunction(x: Int): Int = x + 1

  //  recursion - tail recursion
  def factorial(n: Int, acc: Int): Int =
    if (n <= 0) acc
    else factorial(n - 1, acc * n)


  //  oop
  class Animal

  class Dog extends Animal

  val aDog: Animal = new Dog

  //  A trait with an abstract method (a method that may be unimplemented)
  trait Carnivore {
    def eat(animal: Animal): Unit
  }

  //  you can mix in as many traits
  class Crocodile extends Animal with Carnivore {
    //  you have to implement everything that is abstract from both animal and trait
    override def eat(animal: Animal): Unit = println("Munch!")
  }

  //  method notations - infix method notation
  val aCroc = new Crocodile
  aCroc.eat(aDog)
  //  is identical to
  aCroc eat aDog

  //  anonymous classes - a way to instantiate classes that extend
  //  abstract types without declaring special types
  //  Carnivore is a trait and therefore you cannot instantiate it by itself
  val aCarnivore = new Carnivore {
    override def eat(animal: Animal): Unit = println("Roar!")
  }
  aCarnivore eat aDog

  //  generics
  abstract class MyList[+A]

  //  companion objects
  object MyList

  //  case classes
  case class Person(name: String, age: Int)

  //  Exceptions
  val aPotentialFailure = try {
    throw new RuntimeException("I'm innocent, I swear!") // returns Nothing - No value at all - crashes the JVM
  } catch {
    case e: Exception => "I caught an exception"
  } finally {
    //side effects
    println("Logs")
  }

  //  functional programming - make functions objects
  val incrementer = new Function1[Int, Int] {
    override def apply(v1: Int): Int = v1 + 1
  }

  val incremented = incrementer(30) //  will yield 43
  //  same as incrementer.apply(42)

  //  syntax sugar
  val anonymousIncrementer = (x: Int) => x + 1

  //  Functional programming is about working with functions as first class
  List(1, 2, 3).map(anonymousIncrementer)
  //  Map is a higher order function


  //  for comprehensions
  val pairs = for {
    num <- List(1, 2, 3, 4)
    char <- List('a', 'b', 'c', 'd')
  } yield num + "-" + char
  //  same as List(1,2,3,4).flatMap(num => List('a','b','c','d').map(char => num + "-" + char))

  //  options
  val anOption = Some(2)

  //  try
//  val aTry = Try {
//    throw new RuntimeException
//  }

  //  Pattern Matching
  val unknown = 2
  val order = unknown match {
    case 1 => "first"
    case 2 => "second"
    case _ => "unknown"
  }

  val bob = Person("Bob", 22)
  val greeting = bob match {
    case Person(n, _) => s"Hi, my name is $n"
    case _ => s"Hi, I don't know my name"
  }
}
