package recap


object Multithreading extends App {
  //  creating threads on JVM
  val aThread = new Thread(new Runnable {
    override def run(): Unit = println("I am running in parallel")
  })

  aThread.start() // starting a thread
  aThread.join() // waiting for a thread to finish

  //  syntactic sugar - scala
  val anotherThread = new Thread(() => println("I am also running in parallel"))
  anotherThread.start()

  //  threads are unpredictable
  val threadHello = new Thread(() => (1 to 100).foreach(_ => println("hello")))
  val threadGoodbye = new Thread(() => (1 to 100).foreach(_ => println("goodbye")))
  threadHello.start()
  threadGoodbye.start()

  //  different runs produce different results

  class BankAccount(@volatile private var amount: Int) { // @volatile only solves for atomic reads but writes are still
    // not atomic. Use synchronized
    override def toString: String = "" + amount

    def withdraw(money: Int) = this.amount -= money

    def safeWithdraw(money: Int) = this.synchronized { //  no2 threads can evaluate synchronized at the same time
      this.amount -= money
    }
  }
  // withdraw is NOT atomic
  // this is solved by adding synchronized locks - safeWithdraw


  //  Inter thread communication
  //  This is achieved via the wait-notified mechanism

  //  Futures

  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.Future

  val aFuture = Future {
    //  a long computation that is evaluated on a different thread
    42
  }

  //  callbacks

  import scala.util.{Failure, Success}

  aFuture.onComplete {
    case Success(42) => println("I found the meaning of life")
    case Failure(_) => println("Something happened. life failed")
  }

  //  futures have functional primitives
  val processedFuture = aFuture.map(_ + 1) // will return a Future with the value 43
  val flatFuture = aFuture.flatMap { value =>
    Future(value + 2)
  } //  will result in a Future with the value 44
  val filteredFuture = aFuture.filter(_ % 2 == 0) //will either return the original future or it will fail
  // with an exception

  //  Futures also support for comprehensions
  val anotherFuture = for {
    meaningOfLife <- aFuture
    filteredMeaning <- filteredFuture
  } yield meaningOfLife + filteredMeaning

  // andThen, recover, recoverWith


}
